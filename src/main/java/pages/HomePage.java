package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods {

	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(xpath = "//span[text()='Electronics']") WebElement eleElectronics;
	@FindBy(linkText = "Mi") WebElement eleclickMi;
	@FindBy(xpath = "(//div/button)[2]") WebElement elecloseIcon;
	
	@And("Mouse over on electronics")
	public HomePage mouseOverElectronics() {
		mouseOver(eleElectronics);
		return this;
		
	}
	
	@And("Press esc")
	public HomePage pressEsc(){
		click(elecloseIcon);
		return this;
		
	}
	
	@And("Click mimobiles")
	public MiMobilesPage clickMiMobile() {
		click(eleclickMi);
		return new MiMobilesPage();
		
	}
	
	
}
