package pages;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import wdMethods.SeMethods;

public class Hooks extends SeMethods{
	@Before
	public void before(Scenario sc) {
		String url = "https://www.flipkart.com";
		startResult();
		startTestModule(sc.getName(), sc.getId());
		startTestCase("Test Execution on Flipkart");
		/*test.assignCategory(category);
		test.assignAuthor(authors);*/
		test.assignCategory("Smoke test");
		test.assignAuthor("Naveen");
		startApp("chrome", url);
		/*System.out.println(sc.getName());
		System.out.println(sc.getId());*/
	}
	@After
	public void after(Scenario sc) {
		closeAllBrowsers();
		endResult();
	/*System.out.println(sc.getStatus());
	System.out.println(sc.isFailed());*/
	
	}
	

}
