package pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class MiFirstMobileListPage extends ProjectMethods {

	public MiFirstMobileListPage() {
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(xpath = "//span[@class='_38sUEc']") WebElement eleRatingsReviews;
	
	@And("Verify selected mobile title")
	public MiFirstMobileListPage printTitle() {
//		System.out.println(getTitle());
		verifyPartialTitle("Redmi");
		return this;
	}
	
	@And("Print reviews and ratings")
	public MiFirstMobileListPage getReviewsandRatings() {		
		//System.out.println(getText(locateElement("xpath", "//span[@class='_38sUEc']")));
		System.out.println(getText(eleRatingsReviews));
		return this;
		
		}
	
}
