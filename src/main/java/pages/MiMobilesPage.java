package pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class MiMobilesPage extends ProjectMethods {

	public MiMobilesPage() {
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(xpath = "//span[text()='Electronics']") WebElement eleElectronics;
	@FindBy(xpath = "//div[@class='_1uv9Cb']/div[1]") WebElement eleMiPriceLists;
	@FindBy(xpath = "//div[@class='_1-2Iqu row']/div[1]/div[1]") WebElement eleMiMobilesList;
	@FindBy(xpath = "//div[text()='Newest First']") WebElement eleNewestSort;
	@FindBy(linkText = "Mi") WebElement eleclickMi;
	@FindBy(xpath = "(//div[@class='_1-2Iqu row']/div[1]/div[1])[1]") WebElement eleFirstMobile;
	
	@And("Click sortbynewest")
	public MiMobilesPage sortByNewest() {
		click(eleNewestSort);
		return new MiMobilesPage();
		}
	
	@And("List mobile prices")
	public MiMobilesPage listMobilesPrices() {
		/*List<WebElement> realmeModel = driver.findElementsByXPath("//div[@class='_1-2Iqu row']/div[1]/div[1]");
		List<WebElement> price = driver.findElementsByXPath("//div[@class='_1uv9Cb']/div[1]");*/
		List<WebElement> realmeModel = locateElements("xpath", "//div[@class='_1-2Iqu row']/div[1]/div[1]");
		List<WebElement> price = locateElements("xpath", "//div[@class='_1uv9Cb']/div[1]");
		for(int i=0;i<realmeModel.size();i++) {
			System.out.println("Model = "+realmeModel.get(i).getText()+" Price = "+price.get(i).getText());
		}
		return new MiMobilesPage();
	}
	
	@And("Verify mi mobile title")
	public MiMobilesPage verifyTitle() {
		verifyPartialTitle("Mi Mobile");
		return this;
	}
	
	@And("Click first mobile")
	public MiFirstMobileListPage clickFirstMobile() {
		click(eleFirstMobile);
		switchToWindow(1);
		return new MiFirstMobileListPage();
		
	}
	
}
