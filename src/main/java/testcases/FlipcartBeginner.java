package testcases;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class FlipcartBeginner {
	@Test
	public void excuteFlip() throws InterruptedException {
	//public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.flipkart.com");
		driver.manage().window().maximize();
		driver.findElementByXPath("(//div/button)[2]").click();
		Actions ac = new Actions(driver);
		ac.moveToElement(driver.findElementByXPath("//span[text()='Electronics']")).perform();
		driver.findElementByLinkText("Realme").click();
		Thread.sleep(500);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.titleContains("Mobile Price List"));
		String title = driver.getTitle();
		//String phoneModel = driver.findElementByXPath("(//div[text()[contains(.,'Realme 2')]])[1]").getText();
		if (title.contains("Mobile Price"))
			System.out.println("Real me model is selected");
		else
			System.out.println("Selected is not Realme");
		driver.findElementByXPath("//div[text()='Newest First']").click();
		Thread.sleep(3000);
		//List<WebElement> realmeModel = driver.findElementsByXPath("//div[text()[contains(.,'Realme ')]]");
		List<WebElement> realmeModel = driver.findElementsByXPath("//div[@class='_1-2Iqu row']/div[1]/div[1]");
		List<WebElement> price = driver.findElementsByXPath("//div[@class='_1uv9Cb']/div[1]");
		for(int i=0;i<realmeModel.size();i++) {
			System.out.println("Model = "+realmeModel.get(i).getText()+" Price = "+price.get(i).getText());
		}
		driver.findElementByXPath("(//div[@class='_1-2Iqu row']/div[1]/div[1])[1]").click();
		Set<String> window = driver.getWindowHandles();
		List<String> lst = new ArrayList<>(window);
		driver.switchTo().window(lst.get(1));
		String title2 = driver.getTitle();
		if(title2.contains("Realme C1"))
			System.out.println("Real me C1 is selected");
		else
			System.out.println("Wrong mobile");
		System.out.println(driver.findElementByXPath("//span[@class='_38sUEc']").getText());
		driver.quit();
	}

}
