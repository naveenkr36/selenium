package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.HomePage;
import wdMethods.ProjectMethods;

public class FlipcartByPom extends ProjectMethods {

	@BeforeTest
	public void setData() {
		url = "https://www.flipkart.com";
		testCaseName = "TC001_VerifyMiMobile";
		testDescription = "Testcase will verify the first mobile";
		authors = "Naveen";
		category = "smoke";
		// dataSheetName = "TC001_CreateLead";
		testNodes = "Mobiles";
	}

	@Test()
	public void VerifyMiMobile() {
		new HomePage().pressEsc().mouseOverElectronics().clickMiMobile().verifyTitle().sortByNewest().listMobilesPrices()
		.clickFirstMobile().printTitle().getReviewsandRatings();
	}

}
